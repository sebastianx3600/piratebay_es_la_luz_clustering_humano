package logica;

public class Arista {

	int nroVecino;
	int valor;
	
	public Arista(int numero1, int numero2) {
		nroVecino = numero1;
		valor = numero2;
	}
	
	public int getNumeroVecino() {
		return nroVecino;
	}
	
	public int getValor() {
		return valor;
	}
	
	public boolean equals(Arista t) {
		return nroVecino == t.getNumeroVecino() & valor == t.getValor();
	}
	
}
