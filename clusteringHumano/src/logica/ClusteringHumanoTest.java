package logica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class ClusteringHumanoTest {

	ClusteringHumano ch;
	
	@Before
	public void crearUnCHtest() {
		ch = new ClusteringHumano();
	}
	
	@Test
	public void estaVaciaTest() {
		assertTrue(ch.getCantidadDePersonas() == 0);
	}
	
	@Test
	public void cantidadDePersonasTest() {
		ch.AgregarPersonaNueva("Jose",1,2,3,4);
		ch.AgregarPersonaNueva("Benja",1,2,3,4);
		ch.AgregarPersonaNueva("Paco",1,2,3,4);
		assertTrue(ch.getCantidadDePersonas() == 3);	//cantidadDeVecinos
	}
	
	@Test
	public void agregarUnaPersonaConNombreRepetidoTest() {	//yaEstaAgregadoPersona agregarPersona(Persona)
		ch.AgregarPersonaNueva("Jose",1,2,3,4);		//No permitimos agregar 2 personas con el mismo nombre
		ch.AgregarPersonaNueva("Jose",2,3,4,5);
		assertTrue(ch.getCantidadDePersonas() == 1 && ch.yaEstaAgregadoPersona("Jose"));
	}
	
	@Test
	public void personaSinRelacionesTest() {			//agregarLasRelaciones es privado pero se utiliza en AgregarPersonaNueva
		ch.AgregarPersonaNueva("Marcos",1,2,3,4);
		assertTrue(ch.getRelacionesDe(0).size() == 1);	//Se relaciona solo consigoMismo
	}
	
	@Test
	public void cantidadDeRelacionesTest() {			//agregarLasRelaciones es privado pero se utiliza en AgregarPersonaNueva
		ch.AgregarPersonaNueva("Marcos",1,2,3,4);		//Cuando se ingresa tambien se relaciona consigo mismo
		ch.AgregarPersonaNueva("Gustavo",2,3,4,4);
		assertTrue(ch.getRelacionesDe(0).size() == 2);
	}
	
	//obtenerRelacionesDe
	@Test
	public void obtenerRelacionesDePersonaNroTest() {
		ch.AgregarPersonaNueva("Pehuen", 5, 2, 2, 4);
		ch.AgregarPersonaNueva("Leo", 3, 3, 3, 4);
		ch.AgregarPersonaNueva("Sebas", 2, 4, 3, 4);
		ArrayList<Arista> rel = ch.getRelacionesDe(0);
		assertTrue(rel.get(0).getNumeroVecino() == 0 && rel.get(0).getValor() == 0
				&& rel.get(1).getNumeroVecino() == 1 && rel.get(1).getValor() == 4
				&& rel.get(2).getNumeroVecino() == 2 && rel.get(2).getValor() == 6);
	}
	
	//obtenerListaDePersonas
	@Test
	public void obtenerListaDePersonasCompararQueSiEstanTest() {
		boolean existePehuen = false;
		boolean existeLeo = false;
		boolean existeSebas = false;
		ch.AgregarPersonaNueva("Pehuen", 5, 2, 2, 4);
		ch.AgregarPersonaNueva("Leo", 3, 3, 3, 4);
		ch.AgregarPersonaNueva("Sebas", 2, 4, 3, 4);
		Persona pehuen = new Persona("Pehuen", 5, 2, 2, 4);
		Persona leo = new Persona("Leo", 3, 3, 3, 4);
		Persona sebas = new Persona("Sebas", 2, 4, 3, 4);
		ArrayList<Persona> personas = ch.getListaDePersonas();
		for(Persona p: personas) {
			existePehuen = existePehuen || p.equals(pehuen);
			existeLeo = existeLeo || p.equals(leo);
			existeSebas = existeSebas|| p.equals(sebas);
		}
		assertTrue(existePehuen && existeLeo && existeSebas);
	}
	
	@Test
	public void obtenerListaDePersonasYCompararQueNoEstanTest() {
		boolean existeMaikol = false;
		ch.AgregarPersonaNueva("Pehuen", 5, 2, 2, 4);
		ch.AgregarPersonaNueva("Leo", 3, 3, 3, 4);
		ch.AgregarPersonaNueva("Sebas", 2, 4, 3, 4);
		Persona maikol = new Persona("Maikol", 2, 4, 3, 4);
		ArrayList<Persona> personas = ch.getListaDePersonas();
		for(Persona p: personas) {
			existeMaikol = existeMaikol || p.equals(maikol);
		}
		assertFalse(existeMaikol);
	}
	
	//obtenerRelacionesMinDe
	@Test
	public void obtenerRelacionesMinimasCantidadesTest() {
		ch.AgregarPersonaNueva("Arnold", 1, 2, 3, 4);
		ch.AgregarPersonaNueva("Barbara", 2, 3, 4, 5);
		ch.AgregarPersonaNueva("Carlos", 5, 5, 5, 5);
		assertTrue(ch.getRelacionesMinDe(0).size() == 1 && ch.getRelacionesMinDe(1).size() == 1 
				&& ch.getRelacionesMinDe(2).size() == 0);
	}
	
	//obtenerRelacionesMinDe
	@Test
	public void obtenerRelacionesMinimasValoresTest() {
		ch.AgregarPersonaNueva("Arnold", 1, 2, 3, 4);
		ch.AgregarPersonaNueva("Barbara", 2, 3, 4, 5);
		ch.AgregarPersonaNueva("Carlos", 5, 5, 5, 5);
		assertTrue(ch.getRelacionesMinDe(0).get(0).getNumeroVecino() == 1 &&
				ch.getRelacionesMinDe(0).get(0).getValor() == 4 && 
				ch.getRelacionesMinDe(1).get(0).getNumeroVecino() == 0 && ch.getRelacionesMinDe(1).get(0).getValor() == 4
				);
		
	}
	
	//getComponentesConexas
	@Test
	public void dividirEnDosGruposTest() {
		ch.AgregarPersonaNueva("Arnold", 1, 2, 3, 4);
		ch.AgregarPersonaNueva("Barbara", 2, 3, 4, 5);
		ch.AgregarPersonaNueva("Carlos", 5, 5, 5, 5);
		ch.AgregarPersonaNueva("David", 4, 4, 4, 4);
		ArrayList<ArrayList<Integer>> cc = ch.getComponentesConexas();
		assertTrue(cc.size() == 2);
	}
	
	//getComponentesConexas
	@Test
	public void dividirEnDosGruposCantidadTest() {
		ch.AgregarPersonaNueva("Arnold", 1, 2, 3, 4);
		ch.AgregarPersonaNueva("Barbara", 2, 3, 4, 5);
		ch.AgregarPersonaNueva("Carlos", 5, 5, 5, 5);
		ch.AgregarPersonaNueva("David", 4, 4, 4, 4);
		ArrayList<ArrayList<Integer>> cc = ch.getComponentesConexas();
		assertTrue(cc.get(0).size() == 1 && 
				cc.get(1).size() == 3);
	}
	
	//getComponentesConexas
	@Test
	public void dividirEnDosGruposValoresTest() {
		ch.AgregarPersonaNueva("Arnold", 1, 2, 3, 4);
		ch.AgregarPersonaNueva("Barbara", 2, 3, 4, 5);
		ch.AgregarPersonaNueva("Carlos", 5, 5, 5, 5);
		ch.AgregarPersonaNueva("David", 4, 4, 4, 4);
		ArrayList<ArrayList<Integer>> cc = ch.getComponentesConexas();
		assertTrue(cc.get(0).contains(0) && cc.get(1).contains(1) && cc.get(1).contains(2) && cc.get(1).contains(3));
	}
	
	//getVecinosDeAGM
	@Test
	public void getVecinosDeAGMDeUnGrupoDeUnaPersonaSolaTest() {
		ch.AgregarPersonaNueva("Arnold", 1, 2, 3, 4);
		ch.AgregarPersonaNueva("Barbara", 2, 3, 4, 5);
		ch.AgregarPersonaNueva("Carlos", 5, 5, 5, 5);
		ch.AgregarPersonaNueva("David", 4, 4, 4, 4);
		ArrayList<Integer> vecinos = ch.getVecinosDeAGM(0);
		assertTrue(vecinos.size() == 1 && vecinos.contains(0));
	}
	
	//getVecinosDeAGM
	@Test
	public void getVecinosDeAGMDeUnGrupoDeTresPersonasTest() {
		ch.AgregarPersonaNueva("Arnold", 1, 2, 3, 4);
		ch.AgregarPersonaNueva("Barbara", 2, 3, 4, 5);
		ch.AgregarPersonaNueva("Carlos", 5, 5, 5, 5);
		ch.AgregarPersonaNueva("David", 4, 4, 4, 4);
		ArrayList<Integer> vecinos = ch.getVecinosDeAGM(1);
		assertTrue(vecinos.size() == 2 && vecinos.contains(3) && !vecinos.contains(2) && !vecinos.contains(0));
	}
	
	//quitarAristaMayor
	@Test
	public void quitarAristaMayorTest() {
		//Genero las condiciones para una arista mayor
		ch.AgregarPersonaNueva("A", 1, 1, 1, 1);
		ch.AgregarPersonaNueva("B", 1, 1, 2, 3);
		ch.AgregarPersonaNueva("C", 4, 4, 4, 4);
		ch.AgregarPersonaNueva("D", 5, 5, 5, 5);
		ArrayList<Integer> vecinos = ch.getVecinosDeAGM(0);
		assertFalse(vecinos.contains(2));
		
	}
	
	//getPesoEntre
	@Test
	public void getPesoEntreDosPersonasTest() { //este metodo solo trabaja con el grafoMin
		ch.AgregarPersonaNueva("Arnold", 1, 2, 3, 4);
		ch.AgregarPersonaNueva("Barbara", 2, 3, 4, 5);
		ch.AgregarPersonaNueva("Carlos", 5, 5, 5, 5);
		int peso=ch.getPesoEntre(0, 1);
		assertTrue(peso==4);	
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void getPesoEntreDosPersonasTestConExcepcion() {
		ch.AgregarPersonaNueva("Arnold", 1, 2, 3, 4);
		ch.AgregarPersonaNueva("Barbara", 2, 3, 4, 5);	
		int peso=ch.getPesoEntre(0, 2);

	}
	/*
	 * TEST PARA EL MANEJO DEL JSON
	 */
	
	String archivoTest = "ClusteringHumanoTest";
	@Test
	public void guardarYRecuperarListaDePersonasComparandoALasPersonasTest() {
		Persona p1 = new Persona("A", 1, 1, 1, 1);
		Persona p2 = new Persona("B", 1, 1, 2, 2);
		Persona p3 = new Persona("C", 4, 4, 4, 4);
		Persona p4 = new Persona("D", 5, 5, 5, 5);
		ch.AgregarPersonaNueva("A", 1, 1, 1, 1);
		ch.AgregarPersonaNueva("B", 1, 1, 2, 2);
		ch.AgregarPersonaNueva("C", 4, 4, 4, 4);
		ch.AgregarPersonaNueva("D", 5, 5, 5, 5);
		ch.guardarListaDePersonas(archivoTest);
		ch = new ClusteringHumano();
		ch.recuperarListaDePersonas(archivoTest);
		assertTrue(ch.getPersona(0).equals(p1) && ch.getPersona(1).equals(p2) 
				&& ch.getPersona(2).equals(p3) && ch.getPersona(3).equals(p4));
	}
	
	@Test
	public void guardarYRecuperarListaDePersonasCantidadDePersonasTest() {
		ch.AgregarPersonaNueva("A", 1, 1, 1, 1);
		ch.AgregarPersonaNueva("B", 1, 1, 2, 2);
		ch.AgregarPersonaNueva("C", 4, 4, 4, 4);
		ch.AgregarPersonaNueva("D", 5, 5, 5, 5);
		ch.guardarListaDePersonas(archivoTest);
		ch = new ClusteringHumano();
		ch.recuperarListaDePersonas(archivoTest);
		assertTrue(ch.getCantidadDePersonas() == 4);
	}
	
	@Test
	public void guardarYRecuperarListaDePersonasRelacionesDelGrafoTest() {
		ch.AgregarPersonaNueva("A", 1, 1, 1, 1);
		ch.AgregarPersonaNueva("B", 1, 1, 2, 2);
		ch.AgregarPersonaNueva("C", 4, 4, 4, 4);
		ch.guardarListaDePersonas(archivoTest);
		ch = new ClusteringHumano();
		ch.recuperarListaDePersonas(archivoTest);	//Comparo la informacion del grafo al recuperar los datos
		assertTrue(
				ch.getRelacionesDe(0).get(0).getNumeroVecino() == 0 && ch.getRelacionesDe(0).get(0).getValor() == 0 &&
				ch.getRelacionesDe(0).get(1).getNumeroVecino() == 1 && ch.getRelacionesDe(0).get(1).getValor() == 2 &&
				ch.getRelacionesDe(0).get(2).getNumeroVecino() == 2 && ch.getRelacionesDe(0).get(2).getValor() == 12 &&
				
				ch.getRelacionesDe(1).get(0).getNumeroVecino() == 0 && ch.getRelacionesDe(1).get(0).getValor() == 2 &&
				ch.getRelacionesDe(1).get(1).getNumeroVecino() == 1 && ch.getRelacionesDe(1).get(1).getValor() == 0 &&
				ch.getRelacionesDe(1).get(2).getNumeroVecino() == 2 && ch.getRelacionesDe(1).get(2).getValor() == 10 &&
				
				ch.getRelacionesDe(2).get(0).getNumeroVecino() == 0 && ch.getRelacionesDe(2).get(0).getValor() == 12 &&
				ch.getRelacionesDe(2).get(1).getNumeroVecino() == 1 && ch.getRelacionesDe(2).get(1).getValor() == 10 &&
				ch.getRelacionesDe(2).get(2).getNumeroVecino() == 2 && ch.getRelacionesDe(2).get(2).getValor() == 0
				);
	}
	
	/*
	 * FIN DE TESTS REACIONADOS AL JSON
	 */
	
	//reiniciarLista
	@Test
	public void reiniciarListaTest() {
		ch.AgregarPersonaNueva("A", 1, 1, 1, 1);
		ch.AgregarPersonaNueva("B", 1, 1, 2, 2);
		ch.AgregarPersonaNueva("C", 4, 4, 4, 4);
		ch.AgregarPersonaNueva("D", 5, 5, 5, 5);
		ch.reiniciarLista();
		assertTrue(ch.getListaDePersonas().size() == 0);
	}
	
	//obtenerNombresDePersonas
	@Test
	public void obtenerNombresDePersonasQueFueronAgregadosTest() {
		ch.AgregarPersonaNueva("A", 1, 1, 1, 1);
		ch.AgregarPersonaNueva("B", 1, 1, 2, 2);
		ch.AgregarPersonaNueva("C", 4, 4, 4, 4);
		ch.AgregarPersonaNueva("D", 5, 5, 5, 5);
		assertTrue(ch.getNombresDePersonas().contains("A") && ch.getNombresDePersonas().contains("B") &&
				ch.getNombresDePersonas().contains("C") && ch.getNombresDePersonas().contains("D"));
	}
	
	@Test
	public void obtenerNombresDePersonasQueNoFueronAgregadosTest() {
		ch.AgregarPersonaNueva("A", 1, 1, 1, 1);
		ch.AgregarPersonaNueva("B", 1, 1, 2, 2);
		ch.AgregarPersonaNueva("C", 4, 4, 4, 4);
		ch.AgregarPersonaNueva("D", 5, 5, 5, 5);
		assertFalse(ch.getNombresDePersonas().contains("E"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaMalTest() {
		ch.AgregarPersonaNueva("Kokun",9,2,3,4);
	}
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaMalTest2() {
		ch.AgregarPersonaNueva("Chuerk",1,0,3,4);
	}
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaMalTest3() {
		ch.AgregarPersonaNueva("Franchesco Virgolini",1,2,6,4);
	}
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaMalTest4() {
		ch.AgregarPersonaNueva("Franchesco Virgolini",1,2,3,0);
	}

}
