package interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import logica.ClusteringHumano;
import logica.Persona;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class GrafoDibujo extends JFrame {

	private JPanel contentPane;
	ClusteringHumano ch;
	
	/*
	 *  Funciones y elementos para dibujar los grafos
	 */
	
	public static final int diametroCirculo=60;
	
	int xVerticePadre=50;
	int yVerticePadre=110;
	int xVerticeHijo=this.xVerticePadre;
	int yVerticeHijo=this.yVerticePadre+this.moverY;
	
	int moverX=63;//para moverme a la derecha
	int moverY=85;//para dibujar mas abajo
	int xTexto2;
	ArrayList<ArrayList<Integer>> compConexas;
	ArrayList<VerticePos> verticesPos;
	ArrayList<AristaPos> aristasPos;

	public void guardarValores() {
		int cont=0;
		compConexas=ch.getComponentesConexas(); //devuelve los dos grafos con sus vertices cada uno
		
		verticesPos=new ArrayList<VerticePos>();
		aristasPos = new ArrayList<AristaPos>();

		ArrayList<Integer> graficados=new ArrayList<Integer>();
		
		Point p1;
		Point p2;
		
		for(ArrayList<Integer> grafo: compConexas) {
			for(Integer i: grafo) {	//recorro los vertices de la primer compConexa
				ArrayList<Integer> vecinos= ch.getVecinosDeAGM(i); //obtengo los vecinos del nodo padre , devuelve primero al padre luego a los hijos
				cont=0;
				for(Integer v: vecinos) {
					if(cont==0 && !graficados.contains(v)) { //es padre y no fue graficado solo pasa 1 vez
						Persona aux=ch.getPersona(v);
						verticesPos.add(new VerticePos(xVerticePadre, yVerticePadre,aux,v));
						graficados.add(v);
						this.xTexto2=xVerticePadre; //si tuviesemos mas de 2 compConexas esto ya no sirve
					}
					if(cont!=0 && !graficados.contains(v)) {//es hijo
						
						Persona aux=ch.getPersona(v);
						
						while(yaExisteEsaPosDibujada(xVerticeHijo,yVerticeHijo)) {
							moverXHijo();
						}
						
						verticesPos.add(new VerticePos(xVerticeHijo, yVerticeHijo,aux,v));
						graficados.add(v);
						
						
						
						p1= new Point( (xVerticePadre) , yVerticePadre+ diametroCirculo/2); //punto abajo del padre
						p2 = new Point((xVerticeHijo) , (yVerticeHijo)-diametroCirculo/2); //punto arriba del hijo
						aristasPos.add(new AristaPos(p1.x,p1.y,p2.x, p2.y, ch.getPesoEntre(v, getVertice(xVerticePadre,yVerticePadre).getFila())));						moverXHijo();
					}
					if(graficados.contains(v) && cont==0){//si ya fue dibujado y es la primer iteracion me posiciono
						
						this.xVerticePadre = getVerticePos(v).getX();
						this.yVerticePadre = getVerticePos(v).getY();
						this.yVerticeHijo= yVerticePadre;
						moverYHijo();
						this.xVerticeHijo = xVerticePadre;
					}
					if(graficados.contains(v) && cont!=0) {
					}
					cont++;
				}
				cont=0;
			}
			moverXPadre();
			yVerticeReiniciar();
			cont=0;
		}	
	}
	
	public VerticePos getVerticePos(int fila) {
		for(VerticePos v: verticesPos) {
			if(v.getFila()==fila) {
				return v;
			}
		}
		return null;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawString("Grupo 1", 50, 65); //(String,x,y)
		if(xTexto2 != 50) {
			g.drawString("Grupo 2", xTexto2, 65);
		}
		for(VerticePos v: verticesPos) {
			if(v.getX() >= xTexto2) {
				g.setColor(Color.blue);
				generarVertice(g,v.getX(),v.getY(),v.getP());
			}else {
				g.setColor(Color.RED);
				generarVertice(g,v.getX(),v.getY(),v.getP());
			
		}
		}
		for(AristaPos a: aristasPos) {
			String valor=""+a.getValor();
			if(a.getX1() >= xTexto2) {
				g.setColor(Color.blue);
				generarArista(g,a.getX1(),a.getY1(),a.getX2(),a.getY2());
				g.drawString(valor, (a.getX2()), (a.getY2()));
			}else {
				g.setColor(Color.RED);
				generarArista(g,a.getX1(),a.getY1(),a.getX2(),a.getY2());
				g.drawString(valor, (a.getX2()), (a.getY2()));
			}
			
		}
	}
	
	public void moverY() {
		this.yVerticePadre +=this.moverY;
	}
	public void moverXPadre() {
		this.xVerticePadre +=300;
	}
	public void moverXHijo() {
		 this.xVerticeHijo +=this.moverX;
	}
	public int moverYHijo() {
		return this.yVerticeHijo +=this.moverY;
	}
	public void reinciarXHijo() {
		this.xVerticeHijo=50;
	}
	public void yVerticeReiniciar() {
		this.yVerticePadre=110;
	}
	
	public void generarArista(Graphics g,int x1,int y1, int x2, int y2) {
		g.drawLine(x1, y1, x2, y2);
	}
	
	public void generarVertice(Graphics g, int x, int y,Persona p) {
		g.drawOval(x - diametroCirculo/2 , y - diametroCirculo/2, diametroCirculo, diametroCirculo);
		g.drawString(p.getNombre(), x- diametroCirculo/3, y);
	}
	public boolean yaExisteEsaPosDibujada(int x,int y) {
		for(VerticePos v: verticesPos) {
			if(v.yaExiste(x, y)) {
				return true;
			}
		}
		return false;
	}
	public VerticePos getVertice(int x, int y) {
		for(VerticePos v: verticesPos) {
			if(v.getX()==x && v.getY()==y) {
				return v;
			}
		}return null;
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GrafoDibujo frame = new GrafoDibujo(new ClusteringHumano());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public GrafoDibujo(ClusteringHumano chExterno) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}	
		ch = chExterno;
		guardarValores();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 724, 528);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Archivo");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Cerrar ventana");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	}
}
