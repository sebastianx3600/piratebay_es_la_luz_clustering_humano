package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import logica.Arista;
import logica.ClusteringHumano;
import logica.Persona;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class seleccionar {

	private JFrame frame;
	
	private JTextField nombre;
	ArrayList<ButtonGroup> grupoDeBotones = new ArrayList<ButtonGroup>();
	ArrayList<JRadioButton> radioButtons;
	ClusteringHumano ch = new ClusteringHumano();
	//TABLAS
	private JTable table;
	private JTable table_1;
	JScrollPane scrollPane;
	JScrollPane scrollPane2;
	//LABELS
	JLabel lblNewLabel;
	JLabel lblNewLabel_1;
	JLabel lblNewLabel_2;
	JLabel lblNewLabel_3;
	JLabel lblNewLabel_4;
	JLabel lblNewLabel_5;
	//BOTONES
	JButton btnSiguientePersona;
	JButton btnFinalizar;
	//MENU
	JMenuBar menuBar;
	JMenu mnNewMenu;
	JMenuItem mntmNewMenuItem;
	JMenuItem mntmNewMenuItem_1;
	private JMenuItem mntmNewMenuItem_2;
	private JMenuItem mntmNewMenuItem_3;
	private JMenuItem mntmNewMenuItem_4;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					seleccionar window = new seleccionar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public seleccionar() {
		initialize();
	}
	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1001, 545);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		setearRadioButtons();
		inicializarLabels();
		setearBotones();
		setearTextField();
		setearMenu();
		setearLasTablas();
		actualizarLasTablas();
	}
	
	public void setearRadioButtons() {
		for(int x = 0; x < 5; x++) {
			grupoDeBotones.add(new ButtonGroup());
		}
		radioButtons = new ArrayList<JRadioButton>();
		JRadioButton botonAux;
		int contador = 0;
		int grupo;			//0 para deporte, 1 para musica, 2 espectaculo, 3 ciencia
		int posX;
		int posY;
		for(int ind = 0; ind < 20; ind++) {
			//creador de botones
			contador = ind+1;
			grupo = 0;
			while(contador > 5) {
				contador = contador - 5;
				grupo++;
			}
			botonAux = new JRadioButton(Integer.toString(contador));
			botonAux.setActionCommand(Integer.toString(contador));
			grupoDeBotones.get(grupo).add(botonAux);
			posX = 77 + 40*contador;
			posY = 89 + 42*grupo;
			botonAux.setBounds(posX, posY, 35, 23);
			botonAux.setSelected(true);
			frame.getContentPane().add(botonAux);
		}
	}
	
	public void setearMenu() {
		menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		mnNewMenu = new JMenu("Archivo");
		menuBar.add(mnNewMenu);
		mntmNewMenuItem = new JMenuItem("Recuperar archivos del Json");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ch.recuperarListaDePersonas(ch.getNombreArchivoDefault());
				actualizarLasTablas();
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		mntmNewMenuItem_1 = new JMenuItem("Empezar de nuevo");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ch.reiniciarLista();
				actualizarLasTablas();
			}
		});
		mnNewMenu.add(mntmNewMenuItem_1);
		
		mntmNewMenuItem_4 = new JMenuItem("Guardar");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ch.guardarListaDePersonas(ch.getNombreArchivoDefault());
			}
		});
		mnNewMenu.add(mntmNewMenuItem_4);
		
		mntmNewMenuItem_2 = new JMenuItem("Cerrar sin guardar");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		mnNewMenu.add(mntmNewMenuItem_2);
		mntmNewMenuItem_3 = new JMenuItem("Cerrar y guardar");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ch.guardarListaDePersonas(ch.getNombreArchivoDefault());
				frame.dispose();
			}
		});
		mnNewMenu.add(mntmNewMenuItem_3);
	}
	
	public void inicializarLabels() {
		lblNewLabel_4 = new JLabel("Nombre");
		lblNewLabel_4.setBounds(10, 11, 57, 28);
		frame.getContentPane().add(lblNewLabel_4);
		lblNewLabel_5 = new JLabel("Seleccione segun sus preferencias");
		lblNewLabel_5.setBounds(10, 44, 233, 38);
		frame.getContentPane().add(lblNewLabel_5);
		lblNewLabel_1 = new JLabel("Deporte");
		lblNewLabel_1.setBounds(10, 93, 48, 14);
		frame.getContentPane().add(lblNewLabel_1);
		lblNewLabel = new JLabel("Musica");
		lblNewLabel.setBounds(10, 135, 48, 14);
		frame.getContentPane().add(lblNewLabel);
		lblNewLabel_2 = new JLabel("Noticias del espectaculo");
		lblNewLabel_2.setBounds(10, 176, 117, 14);
		frame.getContentPane().add(lblNewLabel_2);
		lblNewLabel_3 = new JLabel("Ciencia");
		lblNewLabel_3.setBounds(10, 227, 48, 14);
		frame.getContentPane().add(lblNewLabel_3);
	}
	
	public boolean verificarNombreVacio() {
		if(nombre.getText() == null) {
			JOptionPane.showMessageDialog(nombre, "Complete el nombre de la persona");
			return false;
		}else {
			if(nombre.getText().equals("")) {
				JOptionPane.showMessageDialog(nombre, "Complete el nombre de la persona");
				return false;
			}
		}
		return true;
	}
	
	public void setearBotones() {
		btnSiguientePersona = new JButton("Siguiente Persona");
		btnSiguientePersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(verificarNombreVacio()) {
					//AGARRO LOS DATOS INGRESADOS
					int interesDeportes = Integer.parseInt(grupoDeBotones.get(0).getSelection().getActionCommand());
					int interesMusica = Integer.parseInt(grupoDeBotones.get(1).getSelection().getActionCommand());
					int interesEspectaculo = Integer.parseInt(grupoDeBotones.get(2).getSelection().getActionCommand());
					int interesCiencia = Integer.parseInt(grupoDeBotones.get(3).getSelection().getActionCommand());
					String nombreIngresado = nombre.getText();
					//AGREGO A LA PERSONA AL SISTEMA
					ch.AgregarPersonaNueva(nombreIngresado, interesDeportes, interesMusica, interesEspectaculo, interesCiencia);
					actualizarLasTablas();
				}
			}
		});
		btnSiguientePersona.setBounds(10, 283, 156, 43);
		frame.getContentPane().add(btnSiguientePersona);
		btnFinalizar = new JButton("Finalizar");
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ch.guardarListaDePersonas(ch.getNombreArchivoDefault());
				frame.dispose();
			}
		});
		btnFinalizar.setBounds(176, 283, 135, 43);
		frame.getContentPane().add(btnFinalizar);
		
		JButton btnNewButton = new JButton("Graficar el grafo");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GrafoDibujo gg = new GrafoDibujo(ch);
				gg.setVisible(true);
			}
		});
		btnNewButton.setBounds(10, 332, 156, 43);
		frame.getContentPane().add(btnNewButton);
	}
	
	public void setearTextField() {
		nombre = new JTextField();
		nombre.setBounds(77, 15, 166, 20);
		frame.getContentPane().add(nombre);
		nombre.setColumns(10);
	}
	
	public void setearLasTablas() {
		scrollPane = new JScrollPane();
		scrollPane.setBounds(321, 11, 543, 209);
		frame.getContentPane().add(scrollPane);
		table = new JTable();
		table.setBounds(321, 11, 543, 209);
		scrollPane.setViewportView(table);
		frame.getContentPane().add(scrollPane);			
		scrollPane2 = new JScrollPane();				//TABLA MINIMAL
		scrollPane2.setBounds(320, 231, 543, 225);
		frame.getContentPane().add(scrollPane2);
		
		table_1 = new JTable();
		table_1.setBounds(320, 231, 543, 225);
		scrollPane2.setViewportView(table_1);			
		frame.getContentPane().add(scrollPane2);
	}
	
	public void actualizarLasTablas() {
		ArrayList<Arista> relaciones = new ArrayList<Arista>();
		ArrayList<Persona> personas = ch.getListaDePersonas();
		int cantPersonas = ch.getCantidadDePersonas();
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Persona");
		for(int p = 0; p<cantPersonas; p++) {
			model.addColumn(personas.get(p));
		}
		Vector<String> lineaAgregar = new Vector<String> ();
		for(int ind = 0; ind < cantPersonas; ind++) {
			relaciones = ch.getRelacionesDe(ind);
			lineaAgregar = new Vector<String> ();
			lineaAgregar.add(personas.get(ind).toString());
			for(Arista a: relaciones) {
				lineaAgregar.add(Integer.toString(a.getValor()));
			}
			model.addRow(lineaAgregar);
		}
		table.setModel(model);
		model = new DefaultTableModel();		//TABLA MINIMAL
		model.addColumn("Persona");
		for(int p = 0; p<cantPersonas; p++) {
			model.addColumn(personas.get(p));
		}
		lineaAgregar = new Vector<String> ();
		for(int ind = 0; ind < cantPersonas; ind++) {
			relaciones = ch.getRelacionesMinDe(ind);
			lineaAgregar = new Vector<String> ();
			lineaAgregar.add(personas.get(ind).toString());
			for(int nroArista = 0; nroArista < cantPersonas; nroArista++) {
				lineaAgregar.add("");
			}
			for(Arista a: relaciones) {
				lineaAgregar.set(a.getNumeroVecino()+1, Integer.toString(a.getValor()));
			}
			model.addRow(lineaAgregar);
		}
		table_1.setModel(model);
	}
}
